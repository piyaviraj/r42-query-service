package com.r42.query.model.aggregate;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.r42.query.model.filters.FilterCriteria;

public class AnalyticsAggregation {
	
	@JsonInclude(Include.NON_NULL)
	private String message;

	@JsonInclude(Include.NON_NULL)
	private FilterCriteria filters;
	
	@JsonInclude(Include.NON_NULL)
	private Aggregate aggregation;
	
	
	public AnalyticsAggregation() {
		
	}

	public AnalyticsAggregation(String message) {
		super();
		this.message = message;
	}

	public AnalyticsAggregation(FilterCriteria filters, Aggregate aggregation) {
		super();
		this.filters = filters;
		this.aggregation = aggregation;
	}
	
	public AnalyticsAggregation(FilterCriteria filters, String message) {
		super();
		this.filters = filters;
		this.message = message;
	}

	public FilterCriteria getFilters() {
		return filters;
	}

	public void setFilters(FilterCriteria filters) {
		this.filters = filters;
	}

	public Aggregate getAggregation() {
		return aggregation;
	}

	public void setAggregation(Aggregate aggregation) {
		this.aggregation = aggregation;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}
	
	public static AnalyticsAggregation get(FilterCriteria filters, Aggregate aggregation) {
		filters = filters.isVerbose() ? filters : null;
		return aggregation != null ? new AnalyticsAggregation(filters, aggregation) : 
			new AnalyticsAggregation(filters, "No aggregated results are avaialble for the filtered range");
	}
	
}

package com.r42.query.model.aggregate;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class Aggregate {

	@JsonInclude(Include.NON_NULL)
	private Double mean;
	
	@JsonInclude(Include.NON_NULL)
	private Double median;
	
	@JsonInclude(Include.NON_NULL)
	private Double max;
	
	@JsonInclude(Include.NON_NULL)
	private Double min;

	public Aggregate() {
		
	}
	
	public Aggregate(Double mean, Double median, Double max, Double min) {
		super();
		this.mean = mean;
		this.median = median;
		this.max = max;
		this.min = min;
	}

	public Double getMean() {
		return mean;
	}

	public void setMean(Double mean) {
		this.mean = mean;
	}

	public Double getMedian() {
		return median;
	}

	public void setMedian(Double median) {
		this.median = median;
	}

	public Double getMax() {
		return max;
	}

	public void setMax(Double max) {
		this.max = max;
	}

	public Double getMin() {
		return min;
	}

	public void setMin(Double min) {
		this.min = min;
	}
	
	
	
}

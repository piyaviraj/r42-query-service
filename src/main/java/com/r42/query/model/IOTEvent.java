package com.r42.query.model;

import java.math.BigDecimal;
import java.time.OffsetDateTime;
import java.util.Date;

import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import org.springframework.data.mongodb.core.mapping.FieldType;

@Document(collection = "iot_events")
public class IOTEvent {

	private Long eventId;
	
	@Field(targetType = FieldType.DECIMAL128) 
	private BigDecimal value;
	
	private Date timestamp;
	
	private String type;
	
	private String name;
	
	private Long clusterId;

	public Long getEventId() {
		return eventId;
	}

	public void setEventId(Long eventId) {
		this.eventId = eventId;
	}

	public BigDecimal getValue() {
		return value;
	}

	public void setValue(BigDecimal value) {
		this.value = value;
	}

	public Date getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(Date timestamp) {
		this.timestamp = timestamp;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getClusterId() {
		return clusterId;
	}

	public void setClusterId(Long clusterId) {
		this.clusterId = clusterId;
	}
	
	@Override
	public String toString() {
		return "IOTEvent [eventId=" + eventId + ", value=" + value + ", timestamp=" + timestamp + ", type=" + type + ", name=" + name
				+ ", clusterId=" + clusterId + "]";
	}
}

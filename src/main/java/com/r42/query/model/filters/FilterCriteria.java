package com.r42.query.model.filters;

import java.util.Date;
import java.util.Objects;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class FilterCriteria {

	private Date from;
	
	private Date to;
	
	@JsonInclude(Include.NON_NULL)
	private String type;
	
	@JsonInclude(Include.NON_NULL)
	private String clusterId;
	
	@JsonIgnore
	private boolean verbose;
	
	
	public FilterCriteria() {
		
	}

	public FilterCriteria(Date from, Date to, String type, String clusterId, boolean verbose) {
		super();
		this.from = from;
		this.to = to;
		this.type = type;
		this.clusterId = clusterId;
		this.verbose = verbose;
	}
	
	public FilterCriteria(Date from, Date to, String type, String clusterId) {
		super();
		this.from = from;
		this.to = to;
		this.type = type;
		this.clusterId = clusterId;
	}

	public Date getFrom() {
		return from;
	}

	public void setFrom(Date from) {
		this.from = from;
	}

	public Date getTo() {
		return to;
	}

	public void setTo(Date to) {
		this.to = to;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

	public String getClusterId() {
		return clusterId;
	}

	public void setClusterId(String clusterId) {
		this.clusterId = clusterId;
	}
	
	public boolean isVerbose() {
		return verbose;
	}

	public static FilterCriteria valueOf(Date from, Date to, String type, String clusterId, boolean verbose) {
		return new FilterCriteria(from, to, type, clusterId, verbose);
	}
	
	public static FilterCriteria valueOf(Date from, Date to, String type, String clusterId) {
		return new FilterCriteria(from, to, type, clusterId);
	}
	
	public static FilterCriteria valueOf(Date from, Date to, String type) {
		return new FilterCriteria(from, to, type, null);
	}
	
	public static FilterCriteria valueOf(Date from, Date to) {
		return new FilterCriteria(from, to, null, null);
	}
	
	@JsonIgnore
	public boolean isValid() {
		return Objects.nonNull(from) && Objects.nonNull(to) && to.after(from);
	}

	@Override
	public String toString() {
		return "FilterCriteria [from=" + from + ", to=" + to + ", type=" + type + ", clusterId=" + clusterId + "]";
	}
	
	
}

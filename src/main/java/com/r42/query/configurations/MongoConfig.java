package com.r42.query.configurations;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.convert.converter.Converter;
import org.springframework.data.mongodb.config.AbstractMongoClientConfiguration;
import org.springframework.data.mongodb.core.convert.MongoCustomConversions;

import com.r42.query.mongo.converters.BigDecimalToIntegerConverter;
import com.r42.query.mongo.converters.IntegerToBigDecimalConverter;
import com.r42.query.mongo.converters.OffsetDataTimeReadConverter;
import com.r42.query.mongo.converters.OffsetDataTimeWriteConverter;


@Configuration
public class MongoConfig extends AbstractMongoClientConfiguration {
	
	@Value("${spring.data.mongodb.database}")
	private String dbName;

	private final List<Converter<?, ?>> converters = new ArrayList<>();
	
	@Override
	protected String getDatabaseName() {
		return this.dbName;
	}

	@Override
    public MongoCustomConversions customConversions() {
        converters.add(new OffsetDataTimeReadConverter());
        converters.add(new OffsetDataTimeWriteConverter());
        converters.add(new BigDecimalToIntegerConverter());
        converters.add(new IntegerToBigDecimalConverter());
        return new MongoCustomConversions(converters);
    }
	
}

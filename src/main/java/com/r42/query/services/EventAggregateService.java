package com.r42.query.services;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.r42.query.exceptions.BadFilterParamsException;
import com.r42.query.model.aggregate.Aggregate;
import com.r42.query.model.aggregate.AnalyticsAggregation;
import com.r42.query.model.filters.FilterCriteria;
import com.r42.query.repositories.AllAggregateRepository;
import com.r42.query.repositories.MaxAggregateRepository;
import com.r42.query.repositories.MeanAggregateRepository;
import com.r42.query.repositories.MedianAggregateRepository;
import com.r42.query.repositories.MinAggregateRepository;
import com.r42.query.types.AggType;

@Service
public class EventAggregateService {

	@Autowired
	private AllAggregateRepository allAggregateRepository;
	
	@Autowired
	private MeanAggregateRepository meanAggregateRepository;
	
	@Autowired
	private MinAggregateRepository minAggregateRepository;
	
	@Autowired
	private MaxAggregateRepository maxAggregateRepository;
	
	@Autowired
	private MedianAggregateRepository medianAggregateRepository;
	
	
	public AnalyticsAggregation getAggregateValues(AggType aggType, FilterCriteria filter) {
		
		if(Objects.isNull(aggType)) {
			throw new BadFilterParamsException("Invalid AggType: " + aggType);
		}
		
		if(Objects.isNull(filter) || !filter.isValid()) {
			throw new BadFilterParamsException("Invalid 'from' and 'to' date range combination for the FilterCriteria: " + filter);
		}
		
		Aggregate aggregate = null;
		
		if (AggType.ALL.equals(aggType)) {
			aggregate = getAllAggregations(filter);
			
		} else if (AggType.AVG.equals(aggType) || AggType.MEAN.equals(aggType)) {
			aggregate = getMeanAggregations(filter);
			
		} else if (AggType.MIN.equals(aggType) ) {
			aggregate = getMinAggregations(filter);
			
		} else if (AggType.MAX.equals(aggType) ) {
			aggregate = getMaxAggregations(filter);
			
		} else if (AggType.MEDIAN.equals(aggType) ) {
			aggregate = getMedianAggregations(filter);
			
		} else {
			throw new UnsupportedOperationException(String.format("The aggregation type (%s) is not supported", aggType));
		}
		
		return AnalyticsAggregation.get(filter, aggregate);
	}

	public Aggregate getAllAggregations(FilterCriteria filter) {
		
		Aggregate aggregate = null;
		
		if(Objects.isNull(filter.getType()) && Objects.isNull(filter.getClusterId())) {
			aggregate = allAggregateRepository.findAllAggregationsByDateRange(filter.getFrom(), filter.getTo());
			
		} else if (Objects.isNull(filter.getClusterId())) {
			aggregate = allAggregateRepository.findAllAggregationsByDateRangeAndType(filter.getFrom(), filter.getTo(), filter.getType());
			
		} else if (Objects.isNull(filter.getType())) {
			aggregate = allAggregateRepository.findAllAggregationsByDateRangeAndClusterId(filter.getFrom(), filter.getTo(), filter.getClusterId());
			
		} else {
			aggregate = allAggregateRepository.findAllAggregations(filter.getFrom(), filter.getTo(), filter.getType(), filter.getClusterId());
		}
		
		return aggregate;
	}
	
	public Aggregate getMeanAggregations(FilterCriteria filter) {
		
		Aggregate aggregate = null;
		
		if(Objects.isNull(filter.getType()) && Objects.isNull(filter.getClusterId())) {
			aggregate = meanAggregateRepository.findMeanByDateRange(filter.getFrom(), filter.getTo());
			
		} else if (Objects.isNull(filter.getClusterId())) {
			aggregate = meanAggregateRepository.findMeanByDateRangeAndType(filter.getFrom(), filter.getTo(), filter.getType());
			
		} else if (Objects.isNull(filter.getType())) {
			aggregate = meanAggregateRepository.findMeanByDateRangeAndClusterId(filter.getFrom(), filter.getTo(), filter.getClusterId());
			
		} else {
			aggregate = meanAggregateRepository.findMean(filter.getFrom(), filter.getTo(), filter.getType(), filter.getClusterId());
		}
		
		return aggregate;
	}
	
	public Aggregate getMinAggregations(FilterCriteria filter) {
		
		Aggregate aggregate = null;
		
		if(Objects.isNull(filter.getType()) && Objects.isNull(filter.getClusterId())) {
			aggregate = minAggregateRepository.findMinByDateRange(filter.getFrom(), filter.getTo());
			
		} else if (Objects.isNull(filter.getClusterId())) {
			aggregate = minAggregateRepository.findMinByDateRangeAndType(filter.getFrom(), filter.getTo(), filter.getType());
			
		} else if (Objects.isNull(filter.getType())) {
			aggregate = minAggregateRepository.findMinByDateRangeAndClusterId(filter.getFrom(), filter.getTo(), filter.getClusterId());
			
		} else {
			aggregate = minAggregateRepository.findMin(filter.getFrom(), filter.getTo(), filter.getType(), filter.getClusterId());
		}
		
		return aggregate;
	}
	
	public Aggregate getMaxAggregations(FilterCriteria filter) {
		
		Aggregate aggregate = null;
		
		if(Objects.isNull(filter.getType()) && Objects.isNull(filter.getClusterId())) {
			aggregate = maxAggregateRepository.findMaxByDateRange(filter.getFrom(), filter.getTo());
			
		} else if (Objects.isNull(filter.getClusterId())) {
			aggregate = maxAggregateRepository.findMaxByDateRangeAndType(filter.getFrom(), filter.getTo(), filter.getType());
			
		} else if (Objects.isNull(filter.getType())) {
			aggregate = maxAggregateRepository.findMaxByDateRangeAndClusterId(filter.getFrom(), filter.getTo(), filter.getClusterId());
			
		} else {
			aggregate = maxAggregateRepository.findMax(filter.getFrom(), filter.getTo(), filter.getType(), filter.getClusterId());
		}
		
		return aggregate;
	}
	
	public Aggregate getMedianAggregations(FilterCriteria filter) {
		
		Aggregate aggregate = null;
		
		if(Objects.isNull(filter.getType()) && Objects.isNull(filter.getClusterId())) {
			aggregate = medianAggregateRepository.findMedianByDateRange(filter.getFrom(), filter.getTo());
			
		} else if (Objects.isNull(filter.getClusterId())) {
			aggregate = medianAggregateRepository.findMedianByDateRangeAndType(filter.getFrom(), filter.getTo(), filter.getType());
			
		} else if (Objects.isNull(filter.getType())) {
			aggregate = medianAggregateRepository.findMedianByDateRangeAndClusterId(filter.getFrom(), filter.getTo(), filter.getClusterId());
			
		} else {
			aggregate = medianAggregateRepository.findMedian(filter.getFrom(), filter.getTo(), filter.getType(), filter.getClusterId());
		}
		
		return aggregate;
	}
	
}

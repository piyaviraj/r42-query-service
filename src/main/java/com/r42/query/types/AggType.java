package com.r42.query.types;

import java.util.stream.Stream;

/**
 * Represents supported aggregate analytics types
 * @author pranasinghe
 *
 */
public enum AggType {

	ALL("all"), AVG("Avg"), MEDIAN("Median"), MAX("Max"), MIN("Min"), MEAN("Mean");
	
	private String type;

	private AggType(String type) {
		this.type = type;
	}
	
	public String getType() {
		return type;
	}
	
	public static boolean isSupported(String aggType) {
		return Stream.of(values()).map(e-> e.getType().toLowerCase()).anyMatch(p-> p.equalsIgnoreCase(aggType));
	}
	
	public static AggType get(String aggType) {
		return AggType.valueOf(aggType.toUpperCase());
	}
	
	@Override
	public String toString() {
		return getType();
	}
}

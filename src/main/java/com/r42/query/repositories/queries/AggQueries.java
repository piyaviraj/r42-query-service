package com.r42.query.repositories.queries;

public interface AggQueries {
	
	public static final String DATE_RANGE_FILTER_QUERY = "{$match: {'timestamp': {$gte: :#{#from},$lte: :#{#to}}}}";
	
	public static final String EVENT_TYPE_FILTER_QUERY = "{$match: {'type': {$eq: :#{#type}}}}";
	
	public static final String CLUSTER_ID_TYPE_CONVERTER_QUERY = "{'$project': {'_id': 0,'value': 1,'clusterId': { $convert: { input: '$clusterId', to: 'string' }}}}";
	
	public static final String CLUSTER_ID_FILTER_QUERY = "{$match: {'clusterId': {$eq: :#{#clusterId}}}}";
	
	public static final String SORT_BY_VALUE_QUERY = "{'$sort': { 'value': 1 }}";
	
	public static final String MEAN_AGG_VALUE_QUERY = "{'$group': { '_id': 0,  mean: {$avg: '$value'} }}";
	
	public static final String MAX_AGG_VALUE_QUERY = "{'$group': { '_id': 0,  max: {$max: '$value'} }}";
	
	public static final String MIN_AGG_VALUE_QUERY = "{'$group': { '_id': 0,  min: {$min: '$value'} }}";
	
	public static final String ALL_AGG_PIPELINE_STEP_1_QUERY = "{'$group': {'_id': 0,'valueArray': {'$push': '$value'}}}";
	
	public static final String ALL_AGG_PIPELINE_STEP_2_QUERY = "{'$project': {'_id': 0,'valueArray': 1,'size': { '$size': ['$valueArray'] }, 'average': {'$avg': ['$valueArray'] }, 'max': {'$max': ['$valueArray']}, 'min': {'$min': ['$valueArray']}}}";
	
	public static final String MEDIAN_PIPELINE_STEP_2_QUERY = "{'$project': {'_id': 0,'valueArray': 1,'size': { '$size': ['$valueArray'] }}}";
	
	public static final String ALL_AGG_PIPELINE_STEP_3_QUERY = "{'$project': {'valueArray': 1,'isEvenLength': { '$eq': [{ '$mod': ['$size', 2] }, 0 ] },'middlePoint': { '$trunc': { '$divide': ['$size', 2] } },'average': 1,'max': 1,'min': 1}}";

	public static final String ALL_AGG_PIPELINE_STEP_4_QUERY = "{'$project': {'valueArray': 1,'isEvenLength': 1,'middlePoint': 1,'beginMiddle': { '$subtract': [ '$middlePoint', 1] },'endMiddle': '$middlePoint','average': 1,'max': 1,'min': 1}}";
	
	public static final String ALL_AGG_PIPELINE_STEP_5_QUERY = "{'$project': {'valueArray': 1,'middlePoint': 1,'beginValue': { '$arrayElemAt': ['$valueArray', '$beginMiddle'] },'endValue': { '$arrayElemAt': ['$valueArray', '$endMiddle'] },'isEvenLength': 1,'average': 1,'max': 1,'min': 1}}";
	
	public static final String ALL_AGG_PIPELINE_STEP_6_QUERY = "{'$project': {'valueArray': 1,'middlePoint': 1,'middleSum': { '$add': ['$beginValue', '$endValue'] },'isEvenLength': 1,'average': 1,'max': 1,'min': 1}}";
	
	public static final String ALL_AGG_PIPELINE_STEP_7_QUERY = "{'$project': {'median': {'$cond': {if: '$isEvenLength',then: { '$divide': ['$middleSum', 2] },else:  { '$arrayElemAt': ['$valueArray', '$middlePoint'] }}},'mean': '$average','max': 1,'min': 1}}";
}

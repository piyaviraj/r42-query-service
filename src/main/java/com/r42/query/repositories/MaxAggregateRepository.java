package com.r42.query.repositories;

import static com.r42.query.repositories.queries.AggQueries.CLUSTER_ID_FILTER_QUERY;
import static com.r42.query.repositories.queries.AggQueries.CLUSTER_ID_TYPE_CONVERTER_QUERY;
import static com.r42.query.repositories.queries.AggQueries.DATE_RANGE_FILTER_QUERY;
import static com.r42.query.repositories.queries.AggQueries.EVENT_TYPE_FILTER_QUERY;
import static com.r42.query.repositories.queries.AggQueries.MAX_AGG_VALUE_QUERY;
import static com.r42.query.repositories.queries.AggQueries.SORT_BY_VALUE_QUERY;

import java.util.Date;

import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

import com.r42.query.model.IOTEvent;
import com.r42.query.model.aggregate.Aggregate;

public interface MaxAggregateRepository extends MongoRepository<IOTEvent, String> {

	@Aggregation(pipeline = {
			DATE_RANGE_FILTER_QUERY,
			MAX_AGG_VALUE_QUERY
		})
	public Aggregate findMaxByDateRange(@Param("from") Date from, @Param("to") Date to);
	
	@Aggregation(pipeline = {
			DATE_RANGE_FILTER_QUERY,
			EVENT_TYPE_FILTER_QUERY,
			MAX_AGG_VALUE_QUERY
			})
	public Aggregate findMaxByDateRangeAndType(@Param("from") Date from, @Param("to") Date to, @Param("type") String type);
	
	@Aggregation(pipeline = {
			DATE_RANGE_FILTER_QUERY,
			CLUSTER_ID_TYPE_CONVERTER_QUERY,
			CLUSTER_ID_FILTER_QUERY,
			MAX_AGG_VALUE_QUERY
			})
	public Aggregate findMaxByDateRangeAndClusterId(@Param("from") Date from, @Param("to") Date to, @Param("clusterId") String clusterId);
	
	@Aggregation(pipeline = {
			DATE_RANGE_FILTER_QUERY,
			SORT_BY_VALUE_QUERY,
			EVENT_TYPE_FILTER_QUERY,
			CLUSTER_ID_TYPE_CONVERTER_QUERY,
			CLUSTER_ID_FILTER_QUERY,
			MAX_AGG_VALUE_QUERY
			})
	public Aggregate findMax(@Param("from") Date from, @Param("to") Date to, @Param("type") String type, @Param("clusterId") String clusterId);
	
}

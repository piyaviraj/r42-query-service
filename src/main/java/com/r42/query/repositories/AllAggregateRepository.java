package com.r42.query.repositories;

import static com.r42.query.repositories.queries.AggQueries.DATE_RANGE_FILTER_QUERY;
import static com.r42.query.repositories.queries.AggQueries.ALL_AGG_PIPELINE_STEP_1_QUERY;
import static com.r42.query.repositories.queries.AggQueries.ALL_AGG_PIPELINE_STEP_2_QUERY;
import static com.r42.query.repositories.queries.AggQueries.ALL_AGG_PIPELINE_STEP_3_QUERY;
import static com.r42.query.repositories.queries.AggQueries.ALL_AGG_PIPELINE_STEP_4_QUERY;
import static com.r42.query.repositories.queries.AggQueries.ALL_AGG_PIPELINE_STEP_5_QUERY;
import static com.r42.query.repositories.queries.AggQueries.ALL_AGG_PIPELINE_STEP_6_QUERY;
import static com.r42.query.repositories.queries.AggQueries.ALL_AGG_PIPELINE_STEP_7_QUERY;
import static com.r42.query.repositories.queries.AggQueries.SORT_BY_VALUE_QUERY;
import static com.r42.query.repositories.queries.AggQueries.EVENT_TYPE_FILTER_QUERY;
import static com.r42.query.repositories.queries.AggQueries.CLUSTER_ID_FILTER_QUERY;
import static com.r42.query.repositories.queries.AggQueries.CLUSTER_ID_TYPE_CONVERTER_QUERY;

import java.util.Date;

import org.springframework.data.mongodb.repository.Aggregation;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.repository.query.Param;

import com.r42.query.model.IOTEvent;
import com.r42.query.model.aggregate.Aggregate;

public interface AllAggregateRepository extends MongoRepository<IOTEvent, String> {

	@Aggregation(pipeline = {
			DATE_RANGE_FILTER_QUERY,
			SORT_BY_VALUE_QUERY,
			ALL_AGG_PIPELINE_STEP_1_QUERY,
			ALL_AGG_PIPELINE_STEP_2_QUERY,
			ALL_AGG_PIPELINE_STEP_3_QUERY,
			ALL_AGG_PIPELINE_STEP_4_QUERY,
			ALL_AGG_PIPELINE_STEP_5_QUERY,
			ALL_AGG_PIPELINE_STEP_6_QUERY,
			ALL_AGG_PIPELINE_STEP_7_QUERY,
			})
	public Aggregate findAllAggregationsByDateRange(@Param("from") Date from, @Param("to") Date to);
	
	@Aggregation(pipeline = {
			DATE_RANGE_FILTER_QUERY,
			SORT_BY_VALUE_QUERY,
			EVENT_TYPE_FILTER_QUERY,
			ALL_AGG_PIPELINE_STEP_1_QUERY,
			ALL_AGG_PIPELINE_STEP_2_QUERY,
			ALL_AGG_PIPELINE_STEP_3_QUERY,
			ALL_AGG_PIPELINE_STEP_4_QUERY,
			ALL_AGG_PIPELINE_STEP_5_QUERY,
			ALL_AGG_PIPELINE_STEP_6_QUERY,
			ALL_AGG_PIPELINE_STEP_7_QUERY,
			})
	public Aggregate findAllAggregationsByDateRangeAndType(@Param("from") Date from, @Param("to") Date to, @Param("type") String type);
	
	@Aggregation(pipeline = {
			DATE_RANGE_FILTER_QUERY,
			SORT_BY_VALUE_QUERY,
			CLUSTER_ID_TYPE_CONVERTER_QUERY,
			CLUSTER_ID_FILTER_QUERY,
			ALL_AGG_PIPELINE_STEP_1_QUERY,
			ALL_AGG_PIPELINE_STEP_2_QUERY,
			ALL_AGG_PIPELINE_STEP_3_QUERY,
			ALL_AGG_PIPELINE_STEP_4_QUERY,
			ALL_AGG_PIPELINE_STEP_5_QUERY,
			ALL_AGG_PIPELINE_STEP_6_QUERY,
			ALL_AGG_PIPELINE_STEP_7_QUERY,
			})
	public Aggregate findAllAggregationsByDateRangeAndClusterId(@Param("from") Date from, @Param("to") Date to, @Param("clusterId") String clusterId);
	
	@Aggregation(pipeline = {
			DATE_RANGE_FILTER_QUERY,
			SORT_BY_VALUE_QUERY,
			EVENT_TYPE_FILTER_QUERY,
			CLUSTER_ID_TYPE_CONVERTER_QUERY,
			CLUSTER_ID_FILTER_QUERY,
			ALL_AGG_PIPELINE_STEP_1_QUERY,
			ALL_AGG_PIPELINE_STEP_2_QUERY,
			ALL_AGG_PIPELINE_STEP_3_QUERY,
			ALL_AGG_PIPELINE_STEP_4_QUERY,
			ALL_AGG_PIPELINE_STEP_5_QUERY,
			ALL_AGG_PIPELINE_STEP_6_QUERY,
			ALL_AGG_PIPELINE_STEP_7_QUERY,
			})
	public Aggregate findAllAggregations(@Param("from") Date from, @Param("to") Date to, @Param("type") String type, @Param("clusterId") String clusterId);
}

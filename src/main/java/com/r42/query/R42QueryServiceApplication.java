package com.r42.query;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class R42QueryServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(R42QueryServiceApplication.class, args);
	}

}

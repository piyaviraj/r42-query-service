package com.r42.query.exceptions;

public class BadFilterParamsException extends RuntimeException {

	private static final long serialVersionUID = 8800601596447989224L;

	public BadFilterParamsException() {
		super("Bad request params exception");
	}
	
	public BadFilterParamsException(String message) {
		super(message);
	}
	
	public BadFilterParamsException(String message, Throwable t) {
		super(message, t);
	}
}

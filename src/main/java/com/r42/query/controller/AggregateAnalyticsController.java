package com.r42.query.controller;

import java.util.Date;

import org.owasp.encoder.Encode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.r42.query.exceptions.BadFilterParamsException;
import com.r42.query.model.aggregate.AnalyticsAggregation;
import com.r42.query.model.filters.FilterCriteria;
import com.r42.query.services.EventAggregateService;
import com.r42.query.types.AggType;

@RestController
public class AggregateAnalyticsController {
	
	private static final Logger LOG = LoggerFactory.getLogger(AggregateAnalyticsController.class);

	@Autowired
	private EventAggregateService aggregationService;
	
	@GetMapping({"/api/events/analytics/aggregates", "/api/events/analytics/aggregates/{aggType}"})
	public ResponseEntity<AnalyticsAggregation> getAggResultsByFilters(
			@PathVariable(name = "aggType", required = false) String aggType,
			@RequestParam(name = "from", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date from,
			@RequestParam(name = "to", required = true) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date to,
			@RequestParam(name = "type", required = false) String type,
			@RequestParam(name = "clusterId", required = false) String clusterId,
			@RequestParam(name = "verbose", required = false, defaultValue = "false") boolean verbose
			) {
		
		try {
			if(aggType == null) {

				AnalyticsAggregation analyticsAggregation = aggregationService.getAggregateValues(AggType.ALL, 
							FilterCriteria.valueOf(from, to, type, clusterId, verbose));
				
				return ResponseEntity
						.ok()
						.body(analyticsAggregation);
				
			} else if (AggType.isSupported(aggType)) {
				
				AnalyticsAggregation analyticsAggregation = aggregationService.getAggregateValues(AggType.get(aggType), 
						FilterCriteria.valueOf(from, to, type, clusterId, verbose));
				
				return ResponseEntity
						.ok()
						.body(analyticsAggregation);
			}
		} catch (BadFilterParamsException e) {
			LOG.debug(e.getMessage());
			return ResponseEntity
					.badRequest()
					.body(new AnalyticsAggregation(Encode.forJavaScript(e.getMessage())));
		} catch (Exception e) {
			LOG.error("Failure in aggregate calculation", e);
			return ResponseEntity
					.internalServerError()
					.body(new AnalyticsAggregation("Failure in aggregate calculation. Please contact the administrator, if this error persists"));
		}
		
		return ResponseEntity
				.badRequest()
				.body(new AnalyticsAggregation(String.format("Invliad aggregate type: %s", Encode.forJavaScript(aggType))));
	}
	
}

package com.r42.query.mongo.converters;

import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;

public class OffsetDataTimeReadConverter implements Converter<Date, OffsetDateTime> {

	@Override
	public OffsetDateTime convert(Date source) {
		return source.toInstant().atZone(ZoneOffset.UTC).toOffsetDateTime();
	}

}

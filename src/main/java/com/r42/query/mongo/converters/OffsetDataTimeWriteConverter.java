package com.r42.query.mongo.converters;

import java.time.OffsetDateTime;
import java.util.Date;

import org.springframework.core.convert.converter.Converter;

public class OffsetDataTimeWriteConverter implements Converter<OffsetDateTime, Date> {

	@Override
	public Date convert(OffsetDateTime offsetDateTime) {
		return Date.from(offsetDateTime.toInstant());
	}

}

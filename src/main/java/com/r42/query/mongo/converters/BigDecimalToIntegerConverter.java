package com.r42.query.mongo.converters;


import org.bson.types.Decimal128;
import org.springframework.core.convert.converter.Converter;

public class BigDecimalToIntegerConverter implements Converter<Decimal128, Integer> {

    @Override
    public Integer convert(Decimal128 source) {
        return source.intValue();
    }
}

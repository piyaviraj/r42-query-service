package com.r42.query.mongo.converters;

import java.math.BigDecimal;

import org.bson.types.Decimal128;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class IntegerToBigDecimalConverter implements Converter<Integer, Decimal128> {

    @Override
    public Decimal128 convert(Integer source) {
        return new Decimal128(BigDecimal.valueOf(source));
    }
    
}

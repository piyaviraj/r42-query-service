FROM adoptopenjdk/openjdk11:alpine-jre
COPY target/*.jar app.jar

ENV MONGODB_HOST=mongodb 

ENTRYPOINT ["java","-jar","/app.jar"]
# r42-query-service

The query service is responsible to provide analytical result aggregation APIs against sensor data collected via Kafka message broker

The r42-message-source is listening to the Kafka message broker on the topic iot-data and writes data to the Mongo database r42db.iot_events collection.

The r42-query-service is reponsible to query the r42db.iot_events collection on realtime basis and provide data aggregate functionalities over Min, Max, Avg and Median operation against to the collected sensor data.

For further details, please refer to the design documentation.

https://gitlab.com/piyaviraj/r42-query-service/-/blob/main/Solution_Design_Approch(PleaseRead).docx